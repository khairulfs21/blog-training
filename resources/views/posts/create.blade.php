@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-body">
        <form action="" method="post">
            <div class="form-group">
              <label for="">Title</label>
              <input type="text" name="" id="" class="form-control" placeholder="" aria-describedby="helpId">
            </div>
            <div class="form-group">
                <label for="">Body</label>
                <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Add New Post</button>
        </form>
    </div>
</div>
@endsection
