@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <h1>
                Post List
                <a href="" class="btn btn-primary btn-sm float-right">Add New Post</a>
            </h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Title</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">Aina</td>
                        <td>Covid-19 deaths (Nov 16): 53 reported fatalities, uptick in active cases</td>
                        <td>{{ now()->toDateTimeString() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
